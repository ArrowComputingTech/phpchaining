<?php

  class ChainMethods {
    function method1() {
      echo "Hello from method1<br>";
      return $this;
    }

    function method2() {
      echo "Hello from method2<br>";
    }
  }

  $cm = new ChainMethods();
  $cm->method1()->method2();

?>
