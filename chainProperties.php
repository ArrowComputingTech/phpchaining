<?php

  class ChainMethods {
    public $name = "Thomas Evers";

    function method1() {
      echo "Hello from method1<br>";
      $this->name = "Chain Methods";
      return $this;
    }

    function method2() {
      echo "Hello from Method2 ($this->name)<br>";
    }
  }

  $cm = new ChainMethods();
  $cm->method1()->method2();

?>
