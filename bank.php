<?php
  
  class Bank {
    public $accno;
    public $name;
    public $balance;

    function __construct($accno, $name) {
      $this->accno = $accno;
      $this->name = $name;
    }

    function depositAmount($amt) {
      $this->balance += $amt;
      echo "Deposit of {$amt} successful (probably).<br>";
      return $this;
    }

    function deductAmount($amt) {
      $this->balance -= $amt;
      echo "Deduction of {$amt} successful (probably).<br>";
      return $this;
    }

    function checkBalance() {
      echo "Balance for {$this->name} is {$this->balance}.<br>";
      return $this;
    }
  }

$user = new Bank(101, "John Smith");
$user->depositAmount(500.00)->checkBalance();
$user->deductAmount(30.00)->checkBalance();
$user->deductAmount(20.00)->checkBalance();

?>
